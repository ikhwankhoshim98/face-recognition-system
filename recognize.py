import numpy as np
import pickle
import os
import cv2


curr_path = os.getcwd()

# Load face detection model
proto_path = os.path.join(curr_path, 'model', 'deploy.prototxt')
model_path = os.path.join(curr_path, 'model', 'res10_300x300_ssd_iter_140000.caffemodel')
face_detector = cv2.dnn.readNetFromCaffe(prototxt=proto_path, caffeModel=model_path)

# Load face recognition model
recognition_model = os.path.join(curr_path, 'model', 'openface_nn4.small2.v1.t7')
face_recognizer = cv2.dnn.readNetFromTorch(model=recognition_model)

recognizer = pickle.loads(open('recognizer.pickle', "rb").read())
le = pickle.loads(open('le.pickle', "rb").read())

print("Begin face recognition...")
vs = cv2.VideoCapture(0)

while True:

    success, frame = vs.read()

    (h, w) = frame.shape[:2]

    image_blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0), False, False)

    face_detector.setInput(image_blob)
    face_detections = face_detector.forward()

    for i in range(0, face_detections.shape[2]):
        confidence = face_detections[0, 0, i, 2]

        if confidence >= 0.5:
            box = face_detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (X1, Y1, X2, Y2) = box.astype("int")

            face = frame[Y1: Y2, X1: X2]

            (fH, fW) = face.shape[:2]

            face_blob = cv2.dnn.blobFromImage(face, 1.0/255, (96, 96), (0, 0, 0), True, False)

            face_recognizer.setInput(face_blob)
            vec = face_recognizer.forward()

            preds = recognizer.predict_proba(vec)[0]
            j = np.argmax(preds)
            proba = preds[j]
            name = le.classes_[j]

            text = format(name)
            y = Y1*4 if Y1*4 > 10 else Y1*4
            cv2.rectangle(frame, (X1- 20, Y1 - 10), (X2 + 20 , Y2 + 40), (0, 0, 205), 2)
            cv2.rectangle(frame, (X1 - 20, Y1 + 265), (X2 + 20, Y2 + 40), (0, 0, 205), cv2.FILLED)
            cv2.putText(frame, text, (X1 - 20, Y2 + 35), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255), 2)

    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    if key == ord('q'):
        break

cv2.destroyAllWindows()



